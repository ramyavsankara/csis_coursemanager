﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;



namespace csis_coursemanager.Models
{
    public class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        //[ScaffoldColumn(true)]
        //[Required]
        public int CourseID { get; set; }

        [Display]
        public string CourseTitle { get; set; }

        public string CourseNumber { get; set; }

        public string Category { get; set; }

        [Range(1, 10)]
        public int Sections { get; set; }

        public string Courselevel { get; set; }

        public string CourseTerm { get; set; }

        [Range(2, 9)]
        public int CreditRange { get; set; }

        //[DataType(DataType.Url)]
        public string Syllabus { get; set; }

        //[ScaffoldColumn(true)]
        //public int FacultyID { get; set; }
        [ScaffoldColumn(true)]
        [Display(Name = "CourseAssistant Id")]
        public int? CourseAssistantID { get; set; }
        public virtual CourseAssistant CourseAssistant { get; set; }

        public static List<Course> ReadAllFromCSV(string filepath)
        {
            List<Course> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => Course.OneFromCsv(v))
                                         .ToList();
            return lst;
        }


        public static Course OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Course item = new Course();

            int i = 0;
            item.CourseID = Convert.ToInt32(values[i++]);
            item.CourseNumber = Convert.ToString(values[i++]);
            item.CourseTitle = Convert.ToString(values[i++]);            
            item.Category = Convert.ToString(values[i++]);
            item.Sections = Convert.ToInt32(values[i++]);
            item.Courselevel = Convert.ToString(values[i++]);
            item.CourseTerm = Convert.ToString(values[i++]);
            item.CreditRange = Convert.ToInt32(values[i++]);
            item.Syllabus = Convert.ToString(values[i++]);
            //item.FacultyID = Convert.ToInt32(values[i++]);
            item.CourseAssistantID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
