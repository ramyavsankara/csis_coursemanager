﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;


namespace csis_coursemanager.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<ApplicationDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.CourseAssistant.RemoveRange(context.CourseAssistant);
            context.Course.RemoveRange(context.Course);
            context.Faculty.RemoveRange(context.Faculty);
            
            context.Job.RemoveRange(context.Job);
            context.UserReview.RemoveRange(context.UserReview);
            context.UsefulLink.RemoveRange(context.UsefulLink);
            context.SaveChanges();
            SeedCourseAssistantFromCsv(relPath, context);
            SeedCourseFromCsv(relPath, context);
            SeedFacultyFromCsv(relPath, context);
            SeedJobFromCsv(relPath, context);
            SeedUserReviewFromCsv(relPath, context);
            SeedUsefulLinkFromCsv(relPath, context);
        }

        private static void SeedUsefulLinkFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "usefullinks.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            UsefulLink.ReadAllFromCSV(source);
            List<UsefulLink> lst = UsefulLink.ReadAllFromCSV(source);
            context.UsefulLink.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedCourseFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "courseData.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Course.ReadAllFromCSV(source);
            List<Course> lst = Course.ReadAllFromCSV(source);
            context.Course.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedFacultyFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "faculty.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Faculty> lst = Faculty.ReadAllFromCSV(source);
            context.Faculty.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedCourseAssistantFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "courseassistant.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<CourseAssistant> lst = CourseAssistant.ReadAllFromCSV(source);
            context.CourseAssistant.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedJobFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "jobs.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Job> lst = Job.ReadAllFromCSV(source);
            context.Job.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedUserReviewFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "review.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<UserReview> lst = UserReview.ReadAllFromCSV(source);
            context.UserReview.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
}
