﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace csis_coursemanager.Models
{
    public class Faculty
    {
        //[Required]
        //[ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FacultyID { get; set; }        
        
        [Display(Name = "Name")]
        public string Name { get; set; }
        
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string EmailId { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone Number Required!")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNum { get; set; }

        [Display(Name = "Specialization")]
        public string Specialization { get; set; }

        [ScaffoldColumn(true)]
        [Display(Name = "Course Id")]
        public int? CourseID { get; set; }
        public virtual Course Course { get; set; }

        // a list of all courses a Faculty can teach

        public List<Course> CourseList { get; set; }

        public static List<Faculty> ReadAllFromCSV(string filepath)
        {
            List<Faculty> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Faculty.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Faculty OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Faculty item = new Faculty();

            int i = 0;
            item.FacultyID = Convert.ToInt32(values[i++]);
            item.Name = Convert.ToString(values[i++]);            
            item.PhoneNum = Convert.ToString(values[i++]);
            item.EmailId = Convert.ToString(values[i++]);
            item.Specialization = Convert.ToString(values[i++]);
            item.CourseID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
