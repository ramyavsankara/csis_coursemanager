﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace csis_coursemanager.Models
{
    public class UsefulLink
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[Required]
        public int UsefulLinkID { get; set; }

        [Display(Name="LinkName")]
        [Required(ErrorMessage = "Please enter a valid url")]
        public string linkName { get; set; }

        [Display(Name="Hyperlink")]
        [Required(ErrorMessage = "Field can't be empty")]
        [DataType(DataType.Url, ErrorMessage = "Link is missing")]
        public string hyperLink { get; set; }

        [Display(Name="Related Course")]
        public string relatedCourse { get; set; }

        [Display(Name="Description")]
        public string description { get; set; }

        [ScaffoldColumn(true)]
        [Display(Name = "Course Id")]
        public int? CourseID { get; set; }
        public virtual Course Course { get; set; }

        public List<Course> CourseList { get; set; }


        public static List<UsefulLink> ReadAllFromCSV(string filepath)
        {
            List<UsefulLink> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => UsefulLink.OneFromCsv(v))
                                         .ToList();
            return lst;
        }


        public static UsefulLink OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            UsefulLink item = new UsefulLink();

            int i = 0;
            item.UsefulLinkID = Convert.ToInt32(values[i++]);
            item.description = Convert.ToString(values[i++]);
            item.hyperLink = Convert.ToString(values[i++]);
            item.linkName = Convert.ToString(values[i++]);
            item.relatedCourse = Convert.ToString(values[i++]);
            item.CourseID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
