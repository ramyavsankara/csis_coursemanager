﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace csis_coursemanager.Models
{
    public class UserReview
    {
        //[ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserReviewID { get; set; }

        //[Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Course ID")]
        [ScaffoldColumn(true)]
        public int? CourseID { get; set; }
        public virtual Course Course { get; set; }

        [Display(Name = "Course Name")]
        public string CourseName { get; set; }

        //[Required]
        [Display(Name = "User Review")]
        public string Review { get; set; }

        public static List<UserReview> ReadAllFromCSV(string filepath)
        {
            List<UserReview> lst = File.ReadAllLines(filepath).Skip(1).Select(v => UserReview.OneFromCsv(v)).ToList();
            return lst;
        }
        public static UserReview OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            UserReview item = new UserReview();
            int i = 0;
            item.UserReviewID = Convert.ToInt32(values[i++]);
            item.LastName = Convert.ToString(values[i++]);
            item.FirstName = Convert.ToString(values[i++]);
            item.CourseName = Convert.ToString(values[i++]);
            item.Review = Convert.ToString(values[i++]);
            item.CourseID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
