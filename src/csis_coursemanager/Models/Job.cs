﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace csis_coursemanager.Models
{
    public class Job
    {
        //[ScaffoldColumn(false)]
        //[Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobID { get; set; }
        //[Required]
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }
        [Display(Name = "Department")]
        public String department { get; set; }
        //[Required]
        [Range(1, 500)]
        public int Hours { get; set; }
        [Display(Name = "Deadline")]
        public DateTime deadline { get; set; }
        [Required(ErrorMessage = "Field can't be empty")]
        [Display(Name = "To Apply")]
        //[DataType(DataType.Url, ErrorMessage = "Link is missing")]
        public string hyperLink { get; set; }

        [ScaffoldColumn(true)]
        [Display(Name = "Course Id")]
        public int? CourseID { get; set; }
        public virtual Course Course { get; set; }
       
        public static List<Job> ReadAllFromCSV(string filepath)
        {
            List<Job> lst = File.ReadAllLines(filepath).Skip(1).Select(v => Job.OneFromCsv(v)).ToList();
            return lst;
        }
        public static Job OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Job item = new Job();

            int i = 0;

            item.JobID = Convert.ToInt16(values[i++]);
            item.JobTitle = Convert.ToString(values[i++]);
            item.department = Convert.ToString(values[i++]);
            item.Hours = Convert.ToInt16(values[i++]);
            item.deadline = Convert.ToDateTime(values[i++]);
            item.hyperLink = Convert.ToString(values[i++]);
            item.CourseID = Convert.ToInt32(values[i++]);
            return item;
        }
    }
}
