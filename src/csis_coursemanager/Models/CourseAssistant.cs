﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace csis_coursemanager.Models
{
    public class CourseAssistant
    {
        //[ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]       
        public int CourseAssistantID { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public string Position { get; set; }
        
        //[Required]
        [Display(Name = "Email Id")]
        //[DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }
        //[ScaffoldColumn(true)]
        //[Display(Name = "Course Id")]
        //public int? CourseID { get; set; }
        //public virtual Course Course { get; set; }
        
        public static List<CourseAssistant> ReadAllFromCSV(string filepath)
        {
            List<CourseAssistant> lst = File.ReadAllLines(filepath).Skip(1).Select(v => CourseAssistant.OneFromCsv(v)).ToList();
            return lst;
        }
        public static CourseAssistant OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            CourseAssistant item = new CourseAssistant();
            int i = 0;
            item.CourseAssistantID = Convert.ToInt32(values[i++]);
            item.LastName = Convert.ToString(values[i++]);
            item.FirstName = Convert.ToString(values[i++]);
            item.Position = Convert.ToString(values[i++]);
            item.EmailId = Convert.ToString(values[i++]);
            //item.CourseID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
