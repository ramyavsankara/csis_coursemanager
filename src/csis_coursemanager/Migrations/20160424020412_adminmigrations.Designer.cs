using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using csis_coursemanager.Models;

namespace csis_coursemanager.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160424020412_adminmigrations")]
    partial class adminmigrations
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("csis_coursemanager.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("csis_coursemanager.Models.Course", b =>
                {
                    b.Property<int>("CourseID");

                    b.Property<string>("Category");

                    b.Property<int?>("CourseAssistantID");

                    b.Property<string>("CourseNumber");

                    b.Property<string>("CourseTerm");

                    b.Property<string>("CourseTitle");

                    b.Property<string>("Courselevel");

                    b.Property<int>("CreditRange");

                    b.Property<int?>("FacultyFacultyID");

                    b.Property<int>("Sections");

                    b.Property<string>("Syllabus");

                    b.Property<int?>("UsefulLinkUsefulLinkID");

                    b.HasKey("CourseID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.CourseAssistant", b =>
                {
                    b.Property<int>("CourseAssistantID");

                    b.Property<string>("EmailId");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("Position");

                    b.HasKey("CourseAssistantID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.Faculty", b =>
                {
                    b.Property<int>("FacultyID");

                    b.Property<int?>("CourseID");

                    b.Property<string>("EmailId");

                    b.Property<string>("Name");

                    b.Property<string>("PhoneNum")
                        .IsRequired();

                    b.Property<string>("Specialization");

                    b.HasKey("FacultyID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.Job", b =>
                {
                    b.Property<int>("JobID");

                    b.Property<int?>("CourseID");

                    b.Property<int>("Hours");

                    b.Property<string>("JobTitle");

                    b.Property<DateTime>("deadline");

                    b.Property<string>("department");

                    b.Property<string>("hyperLink")
                        .IsRequired();

                    b.HasKey("JobID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.UsefulLink", b =>
                {
                    b.Property<int>("UsefulLinkID");

                    b.Property<int?>("CourseID");

                    b.Property<string>("description");

                    b.Property<string>("hyperLink")
                        .IsRequired();

                    b.Property<string>("linkName")
                        .IsRequired();

                    b.Property<string>("relatedCourse");

                    b.HasKey("UsefulLinkID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.UserReview", b =>
                {
                    b.Property<int>("UserReviewID");

                    b.Property<int?>("CourseID");

                    b.Property<string>("CourseName");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Review");

                    b.HasKey("UserReviewID");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("csis_coursemanager.Models.Course", b =>
                {
                    b.HasOne("csis_coursemanager.Models.CourseAssistant")
                        .WithMany()
                        .HasForeignKey("CourseAssistantID");

                    b.HasOne("csis_coursemanager.Models.Faculty")
                        .WithMany()
                        .HasForeignKey("FacultyFacultyID");

                    b.HasOne("csis_coursemanager.Models.UsefulLink")
                        .WithMany()
                        .HasForeignKey("UsefulLinkUsefulLinkID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.Faculty", b =>
                {
                    b.HasOne("csis_coursemanager.Models.Course")
                        .WithMany()
                        .HasForeignKey("CourseID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.Job", b =>
                {
                    b.HasOne("csis_coursemanager.Models.Course")
                        .WithMany()
                        .HasForeignKey("CourseID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.UsefulLink", b =>
                {
                    b.HasOne("csis_coursemanager.Models.Course")
                        .WithMany()
                        .HasForeignKey("CourseID");
                });

            modelBuilder.Entity("csis_coursemanager.Models.UserReview", b =>
                {
                    b.HasOne("csis_coursemanager.Models.Course")
                        .WithMany()
                        .HasForeignKey("CourseID");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("csis_coursemanager.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("csis_coursemanager.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("csis_coursemanager.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
