using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using System;
using Microsoft.AspNet.Authorization;

namespace csis_coursemanager.Controllers
{
    public class FacultiesController : Controller
    {
        private ApplicationDbContext _context;

        public FacultiesController(ApplicationDbContext context)
        {
            _context = context;    
        }
        
        // GET: Faculties
        public IActionResult Index(string sortOrder, string searchString)
        {
            //var applicationDbContext = _context.Faculty.Include(f => f.Course);
            //return View(applicationDbContext.ToList());
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            //ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";           

            ViewBag.CurrentFilter = searchString;
            var faculties = from s in _context.Faculty
                            select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                faculties = faculties.Where(s => s.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    faculties = faculties.OrderByDescending(s => s.Name);
                    break;
                //case "Date":
                //    students = students.OrderBy(s => s.EnrollmentDate);
                //    break;
                //case "date_desc":
                //    students = students.OrderByDescending(s => s.EnrollmentDate);
                //    break;
                default:
                    faculties = faculties.OrderBy(s => s.Name);
                    break;
            }
            return View(faculties.ToList());
        }

        // GET: Faculties/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Faculty faculty = _context.Faculty.Single(m => m.FacultyID == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }

            return View(faculty);
        }

        // GET: Faculties/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course");
            return View();
        }

        // POST: Faculties/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(Faculty faculty)
        {
            if (ModelState.IsValid)
            {
                _context.Faculty.Add(faculty);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", faculty.CourseID);
            return View(faculty);
        }

        // GET: Faculties/Edit/5
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Faculty faculty = _context.Faculty.Single(m => m.FacultyID == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", faculty.CourseID);
            return View(faculty);
        }

        // POST: Faculties/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(Faculty faculty)
        {
            if (ModelState.IsValid)
            {
                _context.Update(faculty);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", faculty.CourseID);
            return View(faculty);
        }

        // GET: Faculties/Delete/5
        [ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Faculty faculty = _context.Faculty.Single(m => m.FacultyID == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }

            return View(faculty);
        }

        // POST: Faculties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteConfirmed(int id)
        {
            Faculty faculty = _context.Faculty.Single(m => m.FacultyID == id);
            _context.Faculty.Remove(faculty);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
