using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using Microsoft.AspNet.Authorization;
using System;

namespace csis_coursemanager.Controllers
{
    public class CoursesController : Controller
    {
        private ApplicationDbContext _context;

        public CoursesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Courses
        public IActionResult Index(string searchString)
        {
            var courses = from s in _context.Course
                            select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                courses = courses.Where(s => s.CourseTitle.Contains(searchString));
            }
            var applicationDbContext = _context.Course.Include(c => c.CourseAssistant);
            return View(courses.ToList());
        }

        // GET: Courses/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Course course = _context.Course.Single(m => m.CourseID == id);
            if (course == null)
            {
                return HttpNotFound();
            }

            return View(course);
        }

        // GET: Courses/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant");
            return View();
        }

        // POST: Courses/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(Course course)
        {
            if (ModelState.IsValid)
            {
                _context.Course.Add(course);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant", course.CourseAssistantID);
            return View(course);
        }

        // GET: Courses/Edit/5
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Course course = _context.Course.Single(m => m.CourseID == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant", course.CourseAssistantID);
            return View(course);
        }

        // POST: Courses/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(Course course)
        {
            if (ModelState.IsValid)
            {
                _context.Update(course);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant", course.CourseAssistantID);
            return View(course);
        }

        // GET: Courses/Delete/5
        [ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Course course = _context.Course.Single(m => m.CourseID == id);
            if (course == null)
            {
                return HttpNotFound();
            }

            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteConfirmed(int id)
        {
            Course course = _context.Course.Single(m => m.CourseID == id);
            _context.Course.Remove(course);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
