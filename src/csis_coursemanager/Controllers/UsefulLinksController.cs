using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using Microsoft.AspNet.Authorization;
using System;

namespace csis_coursemanager.Controllers
{
    public class UsefulLinksController : Controller
    {
        private ApplicationDbContext _context;

        public UsefulLinksController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: UsefulLinks
        public IActionResult Index()
        {
            return View(_context.UsefulLink.ToList());
        }

        // GET: UsefulLinks/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UsefulLink usefulLink = _context.UsefulLink.Single(m => m.UsefulLinkID == id);
            if (usefulLink == null)
            {
                return HttpNotFound();
            }

            return View(usefulLink);
        }

       

        // GET: UsefulLinks/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: UsefulLinks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(UsefulLink usefulLink)
        {
            if (ModelState.IsValid)
            {
                _context.UsefulLink.Add(usefulLink);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(usefulLink);
        }

        // GET: UsefulLinks/Edit/5
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UsefulLink usefulLink = _context.UsefulLink.Single(m => m.UsefulLinkID == id);
            if (usefulLink == null)
            {
                return HttpNotFound();
            }
            return View(usefulLink);
        }

        // POST: UsefulLinks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(UsefulLink usefulLink)
        {
            if (ModelState.IsValid)
            {
                _context.Update(usefulLink);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(usefulLink);
        }

        // GET: UsefulLinks/Delete/5
        [ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UsefulLink usefulLink = _context.UsefulLink.Single(m => m.UsefulLinkID == id);
            if (usefulLink == null)
            {
                return HttpNotFound();
            }

            return View(usefulLink);
        }

        // POST: UsefulLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteConfirmed(int id)
        {
            UsefulLink usefulLink = _context.UsefulLink.Single(m => m.UsefulLinkID == id);
            _context.UsefulLink.Remove(usefulLink);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
