using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using Microsoft.AspNet.Authorization;

namespace csis_coursemanager.Controllers
{
    public class UserReviewsController : Controller
    {
        private ApplicationDbContext _context;

        public UserReviewsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: UserReviews
        public IActionResult Index()
        {
            var applicationDbContext = _context.UserReview.Include(u => u.Course);
            return View(applicationDbContext.ToList());
        }

        // GET: UserReviews/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UserReview userReview = _context.UserReview.Single(m => m.UserReviewID == id);
            if (userReview == null)
            {
                return HttpNotFound();
            }

            return View(userReview);
        }

        // GET: UserReviews/Create
        //[Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course");
            return View();
        }

        // POST: UserReviews/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        public IActionResult Create(UserReview userReview)
        {
            if (ModelState.IsValid)
            {
                _context.UserReview.Add(userReview);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", userReview.CourseID);
            return View(userReview);
        }

        // GET: UserReviews/Edit/5
        //[Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UserReview userReview = _context.UserReview.Single(m => m.UserReviewID == id);
            if (userReview == null)
            {
                return HttpNotFound();
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", userReview.CourseID);
            return View(userReview);
        }

        // POST: UserReviews/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        public IActionResult Edit(UserReview userReview)
        {
            if (ModelState.IsValid)
            {
                _context.Update(userReview);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", userReview.CourseID);
            return View(userReview);
        }

        // GET: UserReviews/Delete/5
        [ActionName("Delete")]
        //[Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            UserReview userReview = _context.UserReview.Single(m => m.UserReviewID == id);
            if (userReview == null)
            {
                return HttpNotFound();
            }

            return View(userReview);
        }

        // POST: UserReviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        public IActionResult DeleteConfirmed(int id)
        {
            UserReview userReview = _context.UserReview.Single(m => m.UserReviewID == id);
            _context.UserReview.Remove(userReview);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
