using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;

namespace csis_coursemanager.Controllers
{
    public class CourseAssistantsController : Controller
    {
        private ApplicationDbContext _context;

        public CourseAssistantsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: CourseAssistants
        public IActionResult Index()
        {
            return View(_context.CourseAssistant.ToList());
        }

        // GET: CourseAssistants/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CourseAssistant courseAssistant = _context.CourseAssistant.Single(m => m.CourseAssistantID == id);
            if (courseAssistant == null)
            {
                return HttpNotFound();
            }

            return View(courseAssistant);
        }

        // GET: CourseAssistants/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CourseAssistants/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CourseAssistant courseAssistant)
        {
            if (ModelState.IsValid)
            {
                _context.CourseAssistant.Add(courseAssistant);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(courseAssistant);
        }

        // GET: CourseAssistants/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CourseAssistant courseAssistant = _context.CourseAssistant.Single(m => m.CourseAssistantID == id);
            if (courseAssistant == null)
            {
                return HttpNotFound();
            }
            return View(courseAssistant);
        }

        // POST: CourseAssistants/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CourseAssistant courseAssistant)
        {
            if (ModelState.IsValid)
            {
                _context.Update(courseAssistant);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(courseAssistant);
        }

        // GET: CourseAssistants/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CourseAssistant courseAssistant = _context.CourseAssistant.Single(m => m.CourseAssistantID == id);
            if (courseAssistant == null)
            {
                return HttpNotFound();
            }

            return View(courseAssistant);
        }

        // POST: CourseAssistants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            CourseAssistant courseAssistant = _context.CourseAssistant.Single(m => m.CourseAssistantID == id);
            _context.CourseAssistant.Remove(courseAssistant);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
