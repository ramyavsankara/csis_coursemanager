using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using Microsoft.AspNet.Authorization;

namespace csis_coursemanager.Controllers
{
    public class JobsController : Controller
    {
        private ApplicationDbContext _context;

        public JobsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Jobs
        public IActionResult Index()
        {
            var applicationDbContext = _context.Job.Include(j => j.Course);
            return View(applicationDbContext.ToList());
        }

        // GET: Jobs/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Job job = _context.Job.Single(m => m.JobID == id);
            if (job == null)
            {
                return HttpNotFound();
            }

            return View(job);
        }

        // GET: Jobs/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course");
            return View();
        }

        // POST: Jobs/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Job job)
        {
            if (ModelState.IsValid)
            {
                _context.Job.Add(job);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", job.CourseID);
            return View(job);
        }

        // GET: Jobs/Edit/5
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Job job = _context.Job.Single(m => m.JobID == id);
            if (job == null)
            {
                return HttpNotFound();
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", job.CourseID);
            return View(job);
        }

        // POST: Jobs/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Job job)
        {
            if (ModelState.IsValid)
            {
                _context.Update(job);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CourseID"] = new SelectList(_context.Course, "CourseID", "Course", job.CourseID);
            return View(job);
        }

        // GET: Jobs/Delete/5
        [Authorize(Roles = "Admin")]
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Job job = _context.Job.Single(m => m.JobID == id);
            if (job == null)
            {
                return HttpNotFound();
            }

            return View(job);
        }

        // POST: Jobs/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Job job = _context.Job.Single(m => m.JobID == id);
            _context.Job.Remove(job);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
