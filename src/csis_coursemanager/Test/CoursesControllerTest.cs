﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using Microsoft.AspNet.Authorization;
using Xunit;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Microsoft.AspNet.Http.Internal;
using System.Collections.Generic;

namespace csis_coursemanager.Controllers
{
    public class CoursesControllerTest : Controller
    {
        private readonly IServiceProvider _serviceProvider;
        public CoursesControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework().AddInMemoryDatabase().AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void Courses_CreatesView()
        {  
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new CoursesController(dbContext);
            var result = controller.Create(); 
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
        [Fact]
        public void Courses_DetailsViewReturn()
        {

            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new CoursesController(dbContext);
            var result = controller.Details(null);
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
            Assert.NotEqual(result.ToString(), "abc");
            
        }

        [Fact]
        public void Courses_Delete()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new CoursesController(dbContext);
            var result = controller.Delete(null);
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
            Assert.True(true);
        }

        [Fact]
        public void EqualTest()
        {
            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        public void NotEqualTest()
        {
            Assert.NotEqual(5, Add(2, 2));
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 1, 2)]
        [InlineData(1, 2, 3)]
        [InlineData(-1, -1, -2)]
        [InlineData(-1, 1, 0)]
        public void TheoryAddExample(int a, int b, int c)
        {
            Assert.Equal(c, Add(a, b));
        }

        int Add(int x, int y)
        { return x + y; }

        //[Fact]
        //public void Courses_Edit()
        //{
        //    // Arrange
        //    var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
        //    var controller = new CoursesController(dbContext);

        //    // Act
        //     var result = controller.Edit(4) as ViewResult;

        //    // Assert
        //    var viewResult = Assert.IsType<HttpNotFoundResult>(result);
        //    Assert.NotNull(viewResult);

        //}

        //[Fact]
        //public async Task Index_CreatesViewWithCourses()
        //{
        //    // Arrange
        //    var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
        //    CreateTestCourses(numberOfCourses: 10, dbContext: dbContext);

        //    var controller = new CoursesController(dbContext);

        //    // Act
        //    var result = controller.Index();

        //    // Assert
        //    var viewResult = Assert.IsType<ViewResult>(result);
        //    Assert.Null(viewResult.ViewName);

        //    Assert.NotNull(viewResult.ViewData);
        //    var viewModel = Assert.IsType<List<Course>>(viewResult.ViewData.Model);
        //    Assert.Equal(10, viewModel.Count);
        //}

        //private void CreateTestCourses(int numberOfCourses, ApplicationDbContext dbContext)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
