﻿using csis_coursemanager.Controllers;
using csis_coursemanager.Models;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace csis_coursemanager.Test
{
    public class UsefulLinksControllerTest
    {
        private readonly IServiceProvider _serviceProvider;

        public UsefulLinksControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework()
            .AddInMemoryDatabase()
            .AddDbContext<ApplicationDbContext>(
                options => options.UseInMemoryDatabase());
            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void UsefulLinks_CreatesView()
        {
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new UsefulLinksController(dbContext);
            var result = controller.Create();
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }

        [Fact]
        public void UsefulLinks_Delete()
        {
            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new UsefulLinksController(dbContext);

            // Act
            var result = controller.Delete(null);

            // Assert
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);
        }


        [Fact]
        public void UsefulLinks_DetailsViewReturn()
        {

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new UsefulLinksController(dbContext);

            // Act

            var result = controller.Details(null);


            // Assert
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);

            Assert.NotNull(viewResult);
        }

        [Fact]
        public void UsefulLinks_ReturnView()
        {

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new UsefulLinksController(dbContext);

            // Act
            var result = controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.ViewData);
            //Assert.Same("Your contact page.", viewResult.ViewData["Message"]);

        }





    }
}
