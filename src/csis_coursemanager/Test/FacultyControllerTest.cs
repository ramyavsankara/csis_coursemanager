﻿using csis_coursemanager.Models;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csis_coursemanager.Controllers;
using Xunit;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http.Internal;
using Microsoft.Extensions.Logging;
using Xunit.Sdk;

namespace csis_coursemanager.Test
{
    public class FacultyControllerTest
    {
        // use dependency injection - create a local IServiceProvider
        private readonly IServiceProvider _serviceProvider;

        // create a constructor to set things up
        public FacultyControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework()
            .AddInMemoryDatabase()
            .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());
            _serviceProvider = services.BuildServiceProvider();
        } 
        
        private static DbSet<Faculty> CreateTestFaculties(int numberOfFaculties, ApplicationDbContext dbContext)
        {
            var faculties = Enumerable.Range(1, numberOfFaculties).Select(n =>
                 new Faculty()
                 {
                     FacultyID = n,
                     Name = "Name " + n,
                     EmailId = "EmailId " + n,
                     PhoneNum = "PhoneNumber " + n,
                     Specialization = "Specialization" + n,
                 });

            dbContext.AddRange(faculties);
            dbContext.SaveChanges();
            return dbContext.Faculty;
        }

        [Fact] 
         public void Details_ReturnsFacultyDetail()
         { 
             // Arrange 
             var facultyId = 1;

             var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>(); 
             var faculties = CreateTestFaculties(numberOfFaculties: 1, dbContext: dbContext);
             var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
             var controller = new FacultiesController(dbContext);
             // Act 
             var result = controller.Details(facultyId);
             // Assert 
             var viewResult = Assert.IsType<ViewResult>(result); 
             Assert.Null(viewResult.ViewName);
             Assert.NotNull(viewResult.ViewData);
            var viewModel = Assert.IsType<Faculty>(viewResult.ViewData.Model); 
            Assert.NotNull(viewModel.FacultyID);
            Assert.Equal(facultyId, viewModel.FacultyID);
            var phoneNum = faculties.SingleOrDefault(g => g.PhoneNum == viewModel.PhoneNum);
            Assert.NotNull(phoneNum);            
            var specilization = faculties.SingleOrDefault(g => g.Specialization == viewModel.Specialization); 
            Assert.NotNull(specilization); 
        }       

        [Fact] 
         public void Index_CreatesView()
         { 
             // Arrange 
             var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>(); 
             CreateTestFaculties(numberOfFaculties: 3, dbContext: dbContext);
             var controller = new FacultiesController(dbContext); 
             // Act 
             var result = controller.Index("name_desc", " "); 
             // Assert 
             var viewResult = Assert.IsType<ViewResult>(result); 
             Assert.Null(viewResult.ViewName); 
             Assert.NotNull(viewResult.ViewData); 
             var viewModel = Assert.IsType<List<Faculty>>(viewResult.ViewData.Model); 
             Assert.Equal(3, viewModel.Count); 
         }

        [Fact] 
        public void Index_FacultyDetails()
        {
            // Arrange 
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 1, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            var result = controller.Index("name_desc", " ");
            // Assert 
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
            var viewModel = Assert.IsType<List<Faculty>>(viewResult.ViewData.Model);
            Assert.Equal(1, viewModel.Count);
            Assert.NotNull(viewModel);
            Assert.Equal("Name 1",viewModel.ElementAt<Faculty>(0).Name);          
        }

        [Fact]
        public void Index_SortFacultyDetails()
        {
            // Arrange 
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 5, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            var result = controller.Index("name_desc", " ");
            // Assert 
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
            var viewModel = Assert.IsType<List<Faculty>>(viewResult.ViewData.Model);
            Assert.Equal(5, viewModel.Count);
            Assert.NotNull(viewModel);
            Assert.True("EmailId 4" != viewModel.ElementAt<Faculty>(4).EmailId); //since desending order
        }

        [Fact]
        public void Index_SearchFacultyDetails()
        {
            // Arrange 
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 5, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            var result = controller.Index("name_asc", "1");
            // Assert 
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
            var viewModel = Assert.IsType<List<Faculty>>(viewResult.ViewData.Model);            
            Assert.NotNull(viewModel);
            Assert.True("EmailId 1" == viewModel.ElementAt<Faculty>(0).EmailId); //since desending order when it search it should be at the top
        }

        [Fact]
        public void Delete_FacultyDetails()
        {
            // Arrange 
            var facultyId = 4;

            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 5, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            var result = controller.Delete(facultyId);
            // Assert 
            var viewResult = Assert.IsType<ViewResult>(result);              
            var viewModel = Assert.IsType<Faculty>(viewResult.ViewData.Model);           
            Assert.NotNull(viewModel);
            Assert.NotEqual(" ", viewModel.Name);
        }

        [Fact]
        public void Edit_FacultyDetail()
        {
            // Arrange 
            var facultyId = 4;
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 5, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            var result = controller.Edit(facultyId);
            // Assert 
            var viewResult = Assert.IsType<ViewResult>(result);           
            var viewModel = Assert.IsType<Faculty>(viewResult.ViewData.Model);
            viewModel.Specialization = "CS"; 
            Assert.True("CS" == viewModel.Specialization);            
        }

        [Fact]
        public void Edit_FacultyDetails()
        {
            // Arrange 
            var facultyId = 4;
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 5, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            var result = controller.Edit(facultyId);
            // Assert 
            var viewResult = Assert.IsType<ViewResult>(result);
            var viewModel = Assert.IsType<Faculty>(viewResult.ViewData.Model);
            viewModel.Name = "Satya";
            Assert.True("Satya" == viewModel.Name);
            viewModel.Specialization = "Arts";
            Assert.Equal("Arts", viewModel.Specialization);                          
        }

        [Fact]
        public void DeleteConfirmed_FacultyDetail()
        {
            // Arrange 
            var facultyId = 3;

            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var faculties = CreateTestFaculties(numberOfFaculties: 5, dbContext: dbContext);
            var cache = _serviceProvider.GetRequiredService<IMemoryCache>();
            var controller = new FacultiesController(dbContext);
            // Act 
            controller.Delete(facultyId);
            var result = controller.DeleteConfirmed(facultyId);
            // Assert 
            var viewResult = Assert.IsType<RedirectToActionResult>(result);            
            Assert.NotNull(viewResult);
        }

        [Fact]
        public void ExpectExceptionButCodeDoesNotThrow()
        {
            try
            {
                Assert.Throws<ArgumentException>(delegate { });
            }
            catch (AssertActualExpectedException exception)
            {
                Assert.Equal("(No exception was thrown)", exception.Actual);
            }
        }
    }
}
