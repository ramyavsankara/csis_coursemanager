﻿using csis_coursemanager.Controllers;
using csis_coursemanager.Models;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace csis_coursemanager.Test
{
    public class JobsControllerTest : Controller
    {
        private readonly IServiceProvider _serviceProvider;

        public JobsControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework().AddInMemoryDatabase().AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void Jobs_CreatesView()
        {  // return type is void not async task

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new JobsController(dbContext);

            // Act
            var result = controller.Create(); // controller uses sync methods (not async)

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
        
        [Fact]
        public void Jobs_ReturnView()
        {

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new JobsController(dbContext);

            // Act
            var result = controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.ViewData);
            //Assert.Same("Your contact page.", viewResult.ViewData["Message"]);

        }

        [Fact]
        public void Jobs_DetailsView()
        {

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new JobsController(dbContext);

            // Act
            var result = controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.ViewData);
            //Assert.Same("Your contact page.", viewResult.ViewData["Message"]);

        }

      

        

        [Fact]
        public void Jobs_Delete()
        {

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new JobsController(dbContext);

            // Act
            var result = controller.Delete(null);

            // Assert
            var viewResult = Assert.IsType<HttpNotFoundResult>(result);
            Assert.NotNull(viewResult);

            //Assert.Same("Your contact page.", viewResult.ViewData["Message"]);
        }
    }
}
        
