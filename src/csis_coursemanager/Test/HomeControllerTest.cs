﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using csis_coursemanager;
using Xunit;
using csis_coursemanager.Models;
using csis_coursemanager.Controllers;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Mvc;

namespace csis_coursemanager.Test
{
    public class HomeControllerTest
    {
        
        private readonly IServiceProvider _serviceProvider;
       
        public HomeControllerTest()
        {
            var efServiceProvider = new ServiceCollection();
            var services = new ServiceCollection();
            services.AddEntityFramework()
            .AddInMemoryDatabase()
            .AddDbContext<ApplicationDbContext>(
                options => options.UseInMemoryDatabase());
            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void About_CreatesView()
        {  
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new HomeController();            
            var result = controller.About();            
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
      
       

        [Fact]
        public void Contact_CreatesViewWithMessage()
        {            
            var controller = new HomeController();            
            var result = controller.Contact();            
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.ViewData);
            Assert.Same("CSIS Course Manager.", viewResult.ViewData["Message"]);
        }

    }
}
