﻿using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using csis_coursemanager.Models;
using Microsoft.AspNet.Authorization;
using Xunit;

namespace csis_coursemanager.Controllers
{
    public class CoursesControllerTest : Controller
    {
        private readonly IServiceProvider _serviceProvider;

        public CoursesControllerTest()
        {
            var efServiceProvider = new ServiceCollection().AddEntityFrameworkInMemoryDatabase().BuildServiceProvider();

            var services = new ServiceCollection();
            services
                .AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>()
                .AddDbContext<CourseContext>(b => b.UseInMemoryDatabase().UseInternalServiceProvider(efServiceProvider));

            services.AddMvc();

            _serviceProvider = services.BuildServiceProvider();
        }
        [Fact]
        public void Courses_CreatesView()
        {  // return type is void not async task

            // Arrange
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            var controller = new EventsController(dbContext);

            // Act
            var result = controller.Create(); // controller uses sync methods (not async)

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }

        //[Fact]
        //public async Task Index_ReturnsNoCartItems_WhenSessionEmpty()
        //{
        //    // Arrange
        //    var httpContext = new DefaultHttpContext();
        //    httpContext.Session = new TestSession();

        //    var controller = new ShoppingCartController(
        //        _serviceProvider.GetRequiredService<MusicStoreContext>(),
        //        _serviceProvider.GetService<ILogger<ShoppingCartController>>());
        //    controller.ControllerContext.HttpContext = httpContext;

        //    // Act
        //    var result = await controller.Index();

        //    // Assert
        //    var viewResult = Assert.IsType<ViewResult>(result);
        //    Assert.NotNull(viewResult.ViewData);
        //    Assert.Null(viewResult.ViewName);

        //    var model = Assert.IsType<ShoppingCartViewModel>(viewResult.ViewData.Model);
        //    Assert.Equal(0, model.CartItems.Count);
        //    Assert.Equal(0, model.CartTotal);
        //}

        //private ApplicationDbContext _context;
        //public CoursesController(ApplicationDbContext context)
        //{
        //    _context = context;
        //}

        //// GET: Courses
        //public IActionResult Index()
        //{
        //    var applicationDbContext = _context.Course.Include(c => c.CourseAssistant);
        //    return View(applicationDbContext.ToList());
        //}

        //// GET: Courses/Details/5
        //public IActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    Course course = _context.Course.Single(m => m.CourseID == id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(course);
        //}

        //// GET: Courses/Create
        //[Authorize(Roles = "Admin")]
        //public IActionResult Create()
        //{
        //    ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant");
        //    return View();
        //}

        //// POST: Courses/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        //public IActionResult Create(Course course)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Course.Add(course);
        //        _context.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant", course.CourseAssistantID);
        //    return View(course);
        //}

        //// GET: Courses/Edit/5
        //[Authorize(Roles = "Admin")]
        //public IActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    Course course = _context.Course.Single(m => m.CourseID == id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant", course.CourseAssistantID);
        //    return View(course);
        //}

        //// POST: Courses/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        //public IActionResult Edit(Course course)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Update(course);
        //        _context.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewData["CourseAssistantID"] = new SelectList(_context.CourseAssistant, "CourseAssistantID", "CourseAssistant", course.CourseAssistantID);
        //    return View(course);
        //}

        //// GET: Courses/Delete/5
        //[ActionName("Delete")]
        //[Authorize(Roles = "Admin")]
        //public IActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    Course course = _context.Course.Single(m => m.CourseID == id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(course);
        //}

        //// POST: Courses/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        //public IActionResult DeleteConfirmed(int id)
        //{
        //    Course course = _context.Course.Single(m => m.CourseID == id);
        //    _context.Course.Remove(course);
        //    _context.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}
